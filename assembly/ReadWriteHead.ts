import { float_, int_, phase_t, rate_t, sample_t, unsigned_int_ } from "./types/Core";
import { IFadeCurves } from "./types/FadeCurves";
import { Action, IReadWriteHead } from "./types/ReadWriteHead";

export default class implements IReadWriteHead {

    head: Array<u32>

    buf: sample_t      // audio buffer (allocated elsewhere)(deref)
    sr: float_           // sample rate
    start: phase_t      // start/end points
    end: phase_t
    queuedCrossfade: phase_t
    queuedCrossfadeFlag: boolean
    fadeTime: float_     // fade time in seconds
    fadeInc: float_      // linear fade increment per sample

    active: int_         // current active play head index (0 or 1)
    loopFlag: boolean      // set to loop, unset for 1-shot
    pre: float_      // pre-record level
    rec: float_      // record level

    rate: rate_t    // current rate
    // TestBuffers testBuf;
    init(fc: IFadeCurves): void {
        this.start = 0.0
        this.end =

    this.end = 0.0;
    this.active = 0;
    this.rate = 1.0;
    this.setFadeTime(0.1);
    // this.testBuf.init();
    this.queuedCrossfade = 0;
    this.queuedCrossfadeFlag = false;
    this.head[0].init(fc);
    this.head[1].init(fc);
    }
    processSample(inn: sample_t, out: sample_t): void;
    processSampleNoRead(inn: sample_t, out: sample_t): void;
    processSampleNoWrite(inn: sample_t, out: sample_t): void;
    setSampleRate(sr: float_): void;
    setBuffer(buf: sample_t, size: unsigned_int_): void;
    setRate(x: rate_t): void;
    setLoopStartSeconds(x: float_): void;
    setLoopEndSeconds(x: float_): void;
    setFadeTime(secs: float_): void;
    setLoopFlag(val: bool): void;
    setRec(x: float_): void;
    setPre(x: float_): void;
    cutToPos(seconds: float_): void;
    setRecOffsetSamples(d: isize): void;
    getActivePhase(): phase_t;
    getRate(): rate_t;


    // fade in to new position (given in samples)
    // assumption: phase is in range!
    private cutToPhase(newPhase: phase_t): void
    private enqueueCrossfade(newPhase: phase_t): void
    private dequeueCrossfade(): void
    private takeAction(act: Action): void

    private mixFade(x: sample_t, y: sample_t, a: float_, b: float_): sample_t // mix two inputs with phases
    private calcFadeInc(): void

}
