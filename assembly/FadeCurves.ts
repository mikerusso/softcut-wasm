import Interpolate from './Interpolate'
import { float_, int_, unsigned_int_ } from './types/Core'
import { IFadeCurves, Shape } from './types/FadeCurves'

const FPI = 3.1415926535898

// const FadeCurves: IFadeCurves = class {
//
export class FadeCurves implements IFadeCurves {
    private fadeBufSize: unsigned_int_ = 1001

    // record delay and pre window in fade, as proportion of fade time
    private recDelayRatio: float_
    private preWindowRatio: float_
    // minimum record delay/pre window, in frames
    private recDelayMinFrames: unsigned_int_
    private preWindowMinFrames: unsigned_int_

    private recFadeBuf: float_[];
    private preFadeBuf: float_[];

    private recShape: Shape
    private preShape: Shape

    constructor() {

    }

    init(): void {
        this.setPreShape(Shape.Linear)
        this.setRecShape(Shape.Raised)
        this.setMinPreWindowFrames(0)
        this.setMinRecDelayFrames(0)
        this.setPreWindowRatio(1.0 / 8)
        this.setRecDelayRatio(1.0 / (8 * 16))
    }


    // "private":
    private calcPreFade(): void {
        let buf: float_[] = new Array<float_>(this.fadeBufSize)
        const nwp: unsigned_int_ = Math.max(this.preWindowMinFrames,
            <unsigned_int_>(this.preWindowRatio * this.fadeBufSize))
        let i: unsigned_int_ = 0
        let x: float_ = 0
        if (this.preShape == Shape.Sine) {
            const phi: float_ = FPI / <float_>nwp

            while (i < nwp) {
                buf[i++] = Math.cos(x) * 0.5 + 0.5
                x += phi
            }

        } else if (this.preShape == Shape.Linear) {
            const phi: float_ = 1 / <float_>nwp

            while (i < nwp) {
                buf[i++] = 1 - x
                x += phi
            }

        } else if (this.recShape == Shape.Raised) {
            assert(this.preShape == Shape.Raised)
            const phi: float_ = FPI / <float_>nwp * 2
            while (i < nwp) {
                buf[i++] = Math.cos(x)
                x += phi
            }
        } else {
            assert(false, "undefined fade shape")
        }
        while (i < this.fadeBufSize) buf[i++] = 0
        this.preFadeBuf = buf
    }

    private calcRecFade(): void {

        let buf: float_[] = new Array<float_>(this.fadeBufSize)
        let n: unsigned_int_ = this.fadeBufSize - 1

        // build rec-fade curve
        // this will be scaled by base rec level
        const ndr: unsigned_int_ = Math.max(this.recDelayMinFrames,
            <unsigned_int_>(this.recDelayRatio * this.fadeBufSize))
        const nr: unsigned_int_ = n - ndr

        let i: unsigned_int_ = 0
        if (this.recShape == Shape.Sine) {
            const phi: float_ = FPI / nr
            let x: float_ = FPI
            let y: float_ = 0
            while (i < ndr) {
                buf[i++] = y
            }
            while (i < n) {
                y = Math.cos(x) * 0.5 + 0.5
                buf[i++] = y
                x += phi
            }
            buf[n] = 1
        } else if (this.recShape == Shape.Linear) {
            const phi: float_ = 1 / nr
            let x: float_ = 0
            while (i < ndr) {
                buf[i++] = x
            }
            while (i < n) {
                buf[i++] = x
                x += phi
            }
            buf[n] = 1
        } else if (this.recShape == Shape.Raised) {
            const phi: float_ = FPI / (nr * 2)
            let x: float_ = FPI
            let y: float_ = 0
            while (i < ndr) {
                buf[i++] = y;
            }
            while (i < n) {
                y = Math.sin(x) * -1
                buf[i++] = y
                x += phi
            }
            buf[n] = 1
        } else {
            assert(false, "undefined fade shape")
        }

        // memcpy(recFadeBuf, buf, fadeBufSize*sizeof(float));
        this.recFadeBuf = buf
    }

    setRecDelayRatio(x: number): void {
        this.preWindowRatio = x
        this.calcPreFade()
    }

    setPreWindowRatio(x: number): void {
        this.preWindowRatio = x
        this.calcPreFade()
    }

    setMinRecDelayFrames(x: number): void {
        this.recDelayMinFrames = x
        this.calcRecFade()
    }

    setMinPreWindowFrames(x: number): void {
        this.preWindowMinFrames = x
        this.calcPreFade()
    }

    setPreShape(x: Shape): void {
        this.preShape = x
        this.calcPreFade()
    }

    setRecShape(x: Shape): void {
        this.recShape = x
        this.calcRecFade()
    }

    getRecFadeValue(x: number): number {
        return Interpolate.tabLinear(this.recFadeBuf, x, this.fadeBufSize)
    }

    getPreFadeValue(x: number): number {
        return Interpolate.tabLinear(this.preFadeBuf, x, this.fadeBufSize)
    }
}
