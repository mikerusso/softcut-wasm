import {float_, GenericFNumber} from './types/Core'

export default class {
    static hermite(x: GenericFNumber, y0: GenericFNumber, y1: GenericFNumber, y2: GenericFNumber, y3: GenericFNumber){
            return (((0.5 * (y3 - y0) + 1.5 * (y1 - y2)) * x + (y0 - 2.5 * y1 + 2. * y2 - 0.5 * y3)) * x + 0.5 * (y2 - y0)) * x + y1;
    }

    static tabLinear(buf: GenericFNumber[], x: float_, size: i64){
        const fi: float_ = x * (size-2)
        let i: u64 = <u64>fi
        const a: float_ = buf[i]
        const b: float_ = buf[i+1]
        const c: float_ = fi - <float_>i
        return a + c*(b-a)
    }
}
