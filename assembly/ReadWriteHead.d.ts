import { float_, int_, phase_t, rate_t, sample_t } from "./types/Core";
import { IFadeCurves } from "./types/FadeCurves";
import { Action, IReadWriteHead } from "./types/ReadWriteHead";

export default class implements IReadWriteHead {

    head: Array<u32>

    buf: sample_t      // audio buffer (allocated elsewhere)(deref)
    sr: float_           // sample rate
    start: phase_t      // start/end points
    end: phase_t
    queuedCrossfade: phase_t
    queuedCrossfadeFlag: boolean
    fadeTime: float_     // fade time in seconds
    fadeInc: float_      // linear fade increment per sample

    active: int_         // current active play head index (0 or 1)
    loopFlag: boolean      // set to loop, unset for 1-shot
    pre: float_      // pre-record level
    rec: float_      // record level

    rate: rate_t    // current rate
    // TestBuffers testBuf;
    init(fc: IFadeCurves): void {

    }
    processSample(inn: number, out: number): void;
    processSampleNoRead(inn: number, out: number): void;
    processSampleNoWrite(inn: number, out: number): void;
    setSampleRate(sr: number): void;
    setBuffer(buf: number, size: number): void;
    setRate(x: number): void;
    setLoopStartSeconds(x: number): void;
    setLoopEndSeconds(x: number): void;
    setFadeTime(secs: number): void;
    setLoopFlag(val: bool): void;
    setRec(x: number): void;
    setPre(x: number): void;
    cutToPos(seconds: number): void;
    setRecOffsetSamples(d: number): void;
    getActivePhase(): number;
    getRate(): number;


    // fade in to new position (given in samples)
    // assumption: phase is in range!
    private cutToPhase(newPhase: phase_t): void
    private enqueueCrossfade(newPhase: phase_t): void
    private dequeueCrossfade(): void
    private takeAction(act: Action): void

    private mixFade(x: sample_t, y: sample_t, a: float_, b: float_): sample_t // mix two inputs with phases
    private calcFadeInc(): void

}
