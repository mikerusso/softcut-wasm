import { float_ } from "./types/Core"
import { ISvf, t_svf } from "./types/Svf"

const M_PI: float_ = 3.14159265358979323846

export default class implements ISvf {

    private sr: float_
    private fc: float_
    private rq: float_
    private g: float_
    private g1: float_
    private g2: float_
    private g3: float_
    private g4: float_
    private v0: float_
    private v1: float_
    private v2: float_
    private v0z: float_
    private v1z: float_
    private v2z: float_
    private v3: float_
    private lp: float_
    private hp: float_
    private bp: float_
    private br: float_
    private lpMix: float_
    private hpMix: float_
    private bpMix: float_
    private brMix: float_

    getNextSample(x: float_): float_ {
        this.svf_update(x)
        return this.lp * this.lpMix + this.hp * this.hpMix + this.bp
            * this.bpMix + this.br * this.brMix
    }
    setSampleRate(sr: float_): void {
        this.sr = sr
        this.svf_calc_coeffs()
    }
    setFc(fc: float_): void {

        this.fc = (fc > this.sr / 2) ? this.sr / 2 : fc
        this.svf_calc_coeffs()
    }
    setRq(rq: float_): void {
        this.rq = rq
        this.svf_calc_coeffs()
    }
    setLpMix(mix: float_): void { this.lpMix = mix }
    setHpMix(mix: float_): void { this.hpMix = mix }
    setBpMix(mix: float_): void { this.bpMix = mix }
    setBrMix(mix: float_): void { this.brMix = mix }

    getFc(): float_ {
        return this.fc
    }



    private svf_calc_coeffs(): void {
        this.g = <float_>(Math.tan(M_PI * this.fc / this.sr))
        this.g1 = this.g / (1 + this.g * (this.g + this.rq))
        this.g2 = 2.0 * (this.g + this.rq) * this.g1
        this.g3 = this.g * this.g1
        this.g4 = 2.0 * this.g1
    }

    private svf_init(): void {
        this.svf_clear_state()
    }

    private svf_clear_state(): void {
        this.v0z = 0
        this.v1 = 0
        this.v2 = 0
    }


    private svf_update(inn: float_): void {
        this.v0 = inn
        this.v1z = this.v1
        this.v2z = this.v2

        this.v3 = this.v0 + this.v0z - 2.0 * this.v2z;
        this.v1 += this.g1 * this.v3 - this.g2 * this.v1z;
        this.v2 += this.g3 * this.v3 + this.g4 * this.v1z;
        this.v0z = this.v0;
        // output
        this.lp = this.v2;
        this.bp = this.v1;
        this.hp = this.v0 - this.rq * this.v1 - this.v2;
        this.br = this.v0 - this.rq * this.v1;
    }

    // why are these static, and requiring the deref?
    // here, we just flatten the original Svf

    // static void svf_calc_coeffs(t_svf* svf);
    // static void svf_init(t_svf* svf);
    // static void svf_clear_state(t_svf* svf);
    // static void svf_set_sr(t_svf* svf, float sr);
    // static void svf_set_fc(t_svf* svf, float fc);
    // static void svf_set_rq(t_svf* svf, float rq);
    // static void svf_update(t_svf* svf, float in);
}
