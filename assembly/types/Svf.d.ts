import { float_ } from "./Core"
export interface ISvf {

    getNextSample(x: float_): float_
    setSampleRate(sr: float_): void
    setFc(fc: float_): void
    setRq(rq: float_): void
    setLpMix(mix: float_): void
    setHpMix(mix: float_): void
    setBpMix(mix: float_): void
    setBrMix(mix: float_): void

    getFc(): float_
}


export interface t_svf {
    // sample rate
    sr: float_
    // corner frequency in hz
    fc: float_
    // reciprocal of Q in [0,1]
    rq: float_
    // intermediate coefficients
    g: float_
    g1: float_
    g2: float_
    g3: float_
    g4: float_
    // state variables
    v0: float_
    v1: float_
    v2: float_
    v0z: float_
    v1z: float_
    v2z: float_
    v3: float_
    // outputs
    lp: float_ // lowpass
    hp: float_ // highpass
    bp: float_ // bandpass
    br: float_ // bandreject
}
