import { float_, phase_t,  rate_t, sample_t, unsigned_int_ } from './Core';
import { IFadeCurves } from './FadeCurves'

export interface IReadWriteHead {

        // should be derefed
        init( fc: IFadeCurves): void

        // per-sample update functions
        processSample( inn: sample_t,  out: sample_t): void
        processSampleNoRead( inn: sample_t,  out: sample_t): void
        processSampleNoWrite( inn: sample_t,  out: sample_t): void
        setSampleRate( sr: float_): void
    // note: size was originally a uint32_t
        setBuffer( buf: sample_t,  size: unsigned_int_): void
        setRate( x: rate_t): void
        setLoopStartSeconds( x: float_): void
        setLoopEndSeconds( x: float_): void
        setFadeTime( secs: float_): void
        setLoopFlag( val: bool): void
        setRec( x: float_): void
        setPre( x: float_): void
        cutToPos( seconds: float_): void

        setRecOffsetSamples( d: isize): void

        getActivePhase(): phase_t
        getRate(): rate_t

}

export enum State {
    Playing=0,
    Stopped=1,
    FadeIn=2,
    FadeOut=3
}
export enum Action { None, Stop, LoopPos, LoopNeg }
