// because we arent confident this is the right float
export type float_ = f32

export type unsigned_int_ = u32
export type int_ = i32

export type sample_t = f32
export type phase_t = f64
export type rate_t = f64

export interface Atomic<T> {
    store(t: T): T
    load(): T
}

export type GenericFNumber = float_ | phase_t
