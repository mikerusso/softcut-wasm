import { float_, Atomic, PhaseT } from './Core'


interface IVoiceMethods {
    init(): undefined
    setBuffer(buf: float_, numFrames: u8): undefined
    setSampleRate(hz: float_): undefined
    setRate(rate: float_): undefined
    setLoopStart(sec: float_): undefined
    setLoopEnd(sec: float_): undefined
    setLoopFlag(sec: float_): undefined

    setFadeTime(sec: float_): undefined
    setRecLevel(amp: float_): undefined
    setPreLevel(amp: float_): undefined
    setRecFlag(val: bool): undefined
    setPlayFlag(val: bool): undefined
    setPreFilterFc(n: float_): undefined
    setPreFilterRq(n: float_): undefined
    setPreFilterLp(n: float_): undefined
    setPreFilterHp(n: float_): undefined
    setPreFilterBp(n: float_): undefined
    setPreFilterBr(n: float_): undefined
    setPreFilterDry(n: float_): undefined
    setPreFilterFcMod(x: float_): undefined
    setPostFilterFc(n: float_): undefined
    setPostFilterRq(n: float_): undefined
    setPostFilterLp(n: float_): undefined
    setPostFilterHp(n: float_): undefined
    setPostFilterBp(n: float_): undefined
    setPostFilterBr(n: float_): undefined
    setPostFilterDry(n: float_): undefined
    cutToPos(s: float_): undefined
    // process a single channel
    // in and out should be dereferenced pointers
    // in should be 'const'
    processBlockMono(inn: float_, out: float_, numFrames: i32): undefined
    setRecOffset(d: float_): undefined
    setRecPreSlewTime(d: float_): undefined
    setRateSlewTime(d: float_): undefined
    setPhaseQuant(x: float_): undefined
    setPhaseOffset(x: float_): undefined
    getQuantPhase(): PhaseT
    getPlayFlag(): boolean
    getRecFlag(): boolean

    getActivePosition(): float_

    // use this from non-audio threads
    getSavedPosition(): float_

    reset(): undefined

    // "private functions"

    updatePreSvfFc(): undefined
    updateQuantPhase(): undefined

    rawPhase: Atomic<PhaseT>
    quantPhase: Atomic<PhaseT>
}

interface  PrivateVoice {
    // buf:
}
