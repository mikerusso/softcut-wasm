import { float_, int_, unsigned_int_ } from './Core'
export abstract class IFadeCurves {

    // initialize with defaults
    init(): void
    setRecDelayRatio(x: float_): void
    setPreWindowRatio(x: float_): void
    setMinRecDelayFrames(x: int_): void
    setMinPreWindowFrames(x: int_): void
    // set curve shape
    setPreShape(x: Shape): void
    setRecShape(x: Shape): void
    // x is assumed to be in [0,1]
    getRecFadeValue(x: float_): float_

    getPreFadeValue(x: float_): float_

}


// typedef enum { Linear=0, Sine=1, Raised=2 } Shape;
export enum Shape {
    Linear = 0, Sine = 1, Raised = 2
}
